import React from 'react'
import {TextInput} from "react-native-paper";
import {Inputs} from "../styles";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import municipios from "../bases/municipios";
import {pickerStyles} from "../styles/pickers";


const pickerSelectStyles = StyleSheet.create(pickerStyles);

const MunicipioSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    /*DEBUG*/ //console.log('municipios_select');
    /*DEBUG*/ //console.log(props.estado);
    if (!!props.estado) {
        //debugger;
        return <TextInput
            label="Município"
            value={props.selectedValue}
            style={Inputs.textInput}
            labelStyle={Inputs.textInputLabel}
            render={(props2) => (
                <RNPickerSelect
                    onValueChange={onValueChange}
                    placeholder={{label: '', value: null}}
                    useNativeAndroidPickerStyle={false}
                    style={pickerSelectStyles}
                    items={municipios[props.estado]}
                    value={props.selectedValue}
                />
            )}
        />
    } else {
        return <TextInput
            label="Município"
            value={props.selectedValue}
            style={Inputs.textInputCompact}
            labelStyle={Inputs.textInputLabel}
            editable={false}
        />
    }

};

export default MunicipioSelect;
