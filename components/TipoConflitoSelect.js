import React from 'react'
import tipos from "../bases/tipos";
import {TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import {pickerStyles} from "../styles/pickers";
import {Inputs} from "../styles";

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoConflitoSelect = (props) => {
    const onValueChange = (value) => {
        props.onValueChange(value);
    };

    const getTiposConflito = () => {
        return tipos.data.tiposConflito.map(tipoConflito => ({
            ...tipoConflito,
            label: tipoConflito.nome,
            value: tipoConflito.id
        }));
    };

    return <TextInput
        label="Tipo de conflito"
        value={props.selectedValue}
        style={Inputs.textInput}
        labelStyle={Inputs.textInputLabel}
        render={(props2) => (
            <RNPickerSelect
                onValueChange={onValueChange}
                placeholder={{label: '', value: null}}
                useNativeAndroidPickerStyle={false}
                style={pickerSelectStyles}
                items={getTiposConflito()}
                value={props.selectedValue}
            />
        )}
    />
};

export default TipoConflitoSelect;
