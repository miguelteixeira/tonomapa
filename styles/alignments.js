export const defaultMargin = {
    marginLeft: 8,
    marginRight: 8
};

export const smSpaceTop = {
    marginTop: 4
};

export const mdSpaceTop = {
    marginTop: 8
};

export const lgSpaceTop = {
    marginTop: 16
};

export const smSpaceBottom = {
    marginBottom: 4
};

export const mdSpaceBottom = {
    marginBottom: 8
};

export const lgSpaceBottom = {
    marginBottom: 16
};
