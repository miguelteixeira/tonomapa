import gql from 'graphql-tag';

/* REMOTE QUERIES */

const LISTA_TERRITORIOS_FIELDS = `
    id,
    nome,
    municipioReferenciaId,
`;

export const getListaTerritoriosData = (idUsuaria) => gql`
    {
        territorios(idUsuaria:${idUsuaria}) {
            ${LISTA_TERRITORIOS_FIELDS}
        }
    }
`;

const USUARIA_FIELDS = `
    __typename,
    id,
    username,
    fullName,
    codigoUsoCelular,
    cpf,
    organizacoes {
        __typename,
        nome
    },
`;

const TERRITORIO_FIELDS = `
    __typename,
    id,
    nome,
    anoFundacao,
    qtdeFamilias,
    statusId,
    tiposComunidade {
        __typename,
        id
    },
    tipoProducaoId,
    modoDeVidaId,
    municipioReferenciaId,
    anexoAta {
        __typename,
        id,
        nome,
        descricao,
        mimetype,
        fileSize,
        arquivo,
        thumb,
        criacao
    },
    poligono,
    conflitos {
        __typename,
        id,
        nome,
        posicao,
        descricao,
        tipoConflitoId
    },
    areasDeUso {
        __typename,
        id,
        nome,
        posicao,
        descricao,
        tipoAreaDeUsoId
    },
    anexos {
        __typename,
        id,
        nome,
        descricao,
        mimetype,
        fileSize,
        arquivo,
        thumb,
        criacao
    },
    mensagens {
        __typename,
        id,
        texto,
        extra,
        dataEnvio,
        dataRecebimento,
        originIsDevice
    },
    publico
`;

export const getTerritorio = (idTerritorio) => gql`
    {
        currentUser {
            ${USUARIA_FIELDS},
            currentTerritorio(id:${idTerritorio}) {
                ${TERRITORIO_FIELDS}
            }
        }
    }
`;

export const GET_USER_DATA_QUERY = `
    currentUser {
        ${USUARIA_FIELDS},
        currentTerritorio {
            ${TERRITORIO_FIELDS}
        }
    }
`;

export const GET_USER_DATA = gql`{ ${GET_USER_DATA_QUERY} }`;

// export const GET_MENSAGENS_QUERY = `
//     mensagens {
//         __typename
//         id,
//         remetente { id, fullName },
//         destinatario { id, fullName },
//         texto,
//         extra,
//         dataEnvio,
//         dataRecebimento
//    }
// `;

// export const GET_USER_DATA_COM_MENSAGENS = gql`{ ${GET_USER_DATA_QUERY}, ${GET_MENSAGENS_QUERY} }`;

// export const GET_MENSAGENS = gql`{ ${GET_MENSAGENS_QUERY} }`;

export const SEND_USER_DATA = gql`
    mutation updateAppData($currentUser: UsuariaInput){
        updateAppData (input: {currentUser: $currentUser}) {
            token,
            errors,
            success,
            ${GET_USER_DATA_QUERY}
        }
    }
`;

const GET_SETTINGS_QUERY = `
    settings {
        appIntro,
        syncedToDashboard,
        useGPS,
        mapType,
        editaPoligonos,
        appFiles,
        editing,
        territorios,
        unidadeArea
    }
`;
export const GET_SETTINGS = gql`{ ${GET_SETTINGS_QUERY} }`;
