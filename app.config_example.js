export default ({ config }) => {
    const version = {
        version: 0,
        build: 59
    };
    const versionName = version.version.toString() + "." + version.build.toString();
    config.version = versionName;
    config.ios.buildNumber = versionName;
    config.android.versionCode = version.build;
    config.extra = {
        testing: false,
        dashboardEndpoint: "http://tonomapa.eita.org.br",
        maxFileSize: 10*1024*1024, // 10MB, change if you want

        // to render XYZ raster tiles
        xyzRTiles: {
            // the value must be 'true' if you plan to use it
            using: false,
            urlTile: "YOUR XYZ TILE (ex.: http://c.tile.openstreetmap.org/{z}/{x}/{y}.png)"
        }
    };

    // For rendering Google Maps:
    config.android.config.googleMaps.apiKey = "YOUR_API_KEY";
    // For receiving notifications through FireBase:
    config.android.googleServicesFile = "PATH_TO_YOUR_GOOGLESERVICES_FILE";

    return {
        ...config,
    };
};
