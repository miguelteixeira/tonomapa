import React, {useState, useRef} from 'react';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import {Avatar, Button, Paragraph, Text, IconButton} from "react-native-paper";
import SobreScreen from "./SobreScreen";
import TermosDeUsoScreen from "./TermosDeUsoScreen";
import DadosBasicosScreen from "./DadosBasicosScreen";
import MensagensScreen from "./MensagensScreen";
import MapaScreen from "./MapaScreen";
import ConfiguracoesScreen from "./ConfiguracoesScreen";
import {Image, Alert, StyleSheet, View} from "react-native";
import TELAS from "../bases/telas";
import STATUS from "../bases/status";
import colors from "../assets/colors";
import {Layout} from "../styles";
import {AuthContext, carregaSettings, carregaCurrentUser} from "../db/api";
import AnexoListScreen from "./AnexoListScreen";
import AnexoFormScreen from "./AnexoFormScreen";
import Constants from 'expo-constants';
import metrics from "../utils/metrics";

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: metrics.tenWidth*2,
    },
    title: {
        marginTop: metrics.tenWidth*2,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: metrics.tenWidth*1.4,
        lineHeight: metrics.tenWidth*1.4,
    },
    row: {
        marginTop: metrics.tenWidth*2,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: metrics.tenWidth*1.5,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: metrics.tenWidth*0.3,
    },
    drawerSection: {
        marginTop: metrics.tenWidth*1.5,
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: metrics.tenWidth*1.2,
        paddingHorizontal: metrics.tenWidth*1.6,
    },
});

const RightDrawer = createDrawerNavigator();

export default function MainScreen({route}) {
    /*DEBUG*/ //console.log('MainScreen', route.params);
    const [telaAtual, setTelaAtual] = useState(TELAS.MAPA);
    const [comunidade, setComunidade] = useState('');
    const [syncedToDashboard, setSyncedToDashboard] = useState(false);
    if (route.params?.telaAtual && telaAtual != route.params.telaAtual) {
        //route.state.index = route.params.telaAtual;
        setTelaAtual(route.params.telaAtual);
        route.params.telaAtual = null;
    }

    carregaSettings().then((resultSettings) => {
        setSyncedToDashboard(resultSettings.syncedToDashboard && resultSettings.syncedToDashboard != STATUS.TELEFONE_NAO_REGISTRADO);
        carregaCurrentUser().then(currentUser => {
            setComunidade(currentUser.currentTerritorio);
        });
    }).catch((errorSettings) => {
        console.log('erro ao carregar settings', errorSettings);
        setSyncedToDashboard(false);
    });

    function CustomDrawerContent(props) {

        /*
        <View>
            <Image source={require('../assets/tonomapa_logo.png')} style={Layout.drawerLogoImage}
                   resizeMode="contain"/>
        </View>
        */

        const {signOut} = React.useContext(AuthContext);

        /*DEBUG*/ //console.log('tonomapa: MainScreen');

        return (
            <DrawerContentScrollView {...props}>
                <View style={Layout.drawerHeader}>
                    <Image source={require('../assets/images/logo/main/tonomapa_logo.png')} style={Layout.drawerLogoImage} resizeMode="contain"/>
                    <Text style={Layout.drawerHeaderLabel}>{comunidade.nome}</Text>
                    <IconButton icon="swap-horizontal-bold" color={colors.primary} onPress={() => {props.navigation.navigate('Configuracoes', {atualizarDados: true, editaComunidades: true})}} />
                </View>
                <DrawerItem label={"Configurações"} labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="settings" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.CONFIGURACOES} onPress={() => {
                    props.navigation.navigate('Configuracoes', {atualizarDados: true});
                }}/>

                <DrawerItem label={"Sobre o Tô no mapa"} labelStyle={{color: colors.text, fontSize: metrics.tenWidth*1.6}}
                            icon={() => (<MaterialCommunityIcons name="information" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.SOBRE} onPress={() => {
                    props.navigation.jumpTo('Sobre');
                }}/>

                <DrawerItem label={"Dados da comunidade"} labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="tooltip-account" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.DADOS_BASICOS} onPress={() => {
                    props.navigation.navigate('DadosBasicos', {atualizarDados: true});
                }}/>


                <DrawerItem label={"Mapear o território"} labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="map" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.MAPEAR_TERRITORIO} onPress={() => {
                    props.navigation.navigate('Home', {tela: TELAS.MAPEAR_TERRITORIO});
                }}/>

                <DrawerItem label={"Usos e conflitos"} labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="map-marker" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.MAPEAR_USOSECONFLITOS} onPress={() => {
                    props.navigation.navigate('Home', {tela: TELAS.MAPEAR_USOSECONFLITOS});
                }}/>

                <DrawerItem label="Arquivos e imagens" labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="file" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.ARQUIVOS} onPress={() => {
                    props.navigation.navigate('AnexoList', {atualizarDados: true});
                }}/>

                <DrawerItem label={"Enviar ao Tô no mapa!"}
                            style={(syncedToDashboard) ? Layout.drawerItem : Layout.drawerEnviar}
                            labelStyle={(syncedToDashboard) ? Layout.drawerLabelDisabled : Layout.drawerEnviarLabel}
                            icon={() => (<MaterialCommunityIcons name="upload" style={(syncedToDashboard) ? Layout.drawerIconDisabled : Layout.drawerEnviarIcon}/>)}
                            focused={telaAtual == TELAS.ENVIAR} onPress={() => {
                    props.navigation.navigate('Home', {tela: TELAS.ENVIAR});
                }}/>
                <DrawerItem label="Mensagens" labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="message" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.MENSAGENS} onPress={() => {
                    if (comunidade.id) {
                        props.navigation.navigate('Mensagens', {atualizarDados: true});
                    } else {
                        Alert.alert(
                            'Território ainda não cadastrado no Tô no Mapa',
                            'Você só poderá receber e enviar mensagens à equipe Tô no Mapa após ter enviado os dados pela primeira vez para o Tô no Mapa.',
                            [
                                {
                                    text: 'Ir a Enviar Dados', onPress: async () => {
                                        props.navigation.navigate('Home', {tela: TELAS.ENVIAR});
                                    }
                                },
                                {
                                    text: 'Ok', onPress: async () => {
                                    }
                                },
                            ],
                            {cancelable: true},
                        );
                    }
                }}/>

                <DrawerItem label="Início" activeTintColor="#FFF" labelStyle={Layout.drawerLabel}
                            icon={() => (<MaterialCommunityIcons name="home" style={Layout.drawerIcon}/>)}
                            focused={telaAtual == TELAS.MAPA} onPress={() => {
                    props.navigation.navigate('Home', {tela: TELAS.MAPA});
                }}/>
                {Constants.manifest.extra.testing &&
                    <>
                    <DrawerItem
                        label="Testes: ver tour inicial" activeTintColor="#FFF"
                        labelStyle={Layout.drawerLabelDisabled}
                        icon={() => (<MaterialCommunityIcons name="ship-wheel"
                        style={Layout.drawerIconDisabled}/>)}
                        focused={telaAtual == TELAS.MAPA} onPress={() => {
                            props.navigation.navigate('Home', {tela: TELAS.MAPA, appIntro: true});
                        }}
                    />

                    <DrawerItem
                        label="Testes: sair"
                        activeTintColor="#FFF"
                        labelStyle={{...Layout.drawerLabel, color:"#999"}}
                        icon={() => (<MaterialCommunityIcons name="logout"
                        style={{...Layout.drawerIcon, color:"#999"}}/>)}
                        onPress={() => {signOut()}}
                    />
                    </>
                }

            </DrawerContentScrollView>
        );
    }

    return (
        <RightDrawer.Navigator
            drawerStyle={Layout.drawer}
            initialRouteName="Home"
            drawerPosition="right"
            edgeWidth={0}
            drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
            <RightDrawer.Screen name="Configuracoes" component={ConfiguracoesScreen}/>
            <RightDrawer.Screen name="Sobre" component={SobreScreen}/>
            <RightDrawer.Screen name="TermosDeUso" component={TermosDeUsoScreen}/>
            <RightDrawer.Screen name="DadosBasicos" component={DadosBasicosScreen}/>
            <RightDrawer.Screen name="Mapa" component={MapaScreen}/>
            <RightDrawer.Screen name="UsosEConflitos" component={MapaScreen}/>
            <RightDrawer.Screen name="Mensagens" component={MensagensScreen}/>
            <RightDrawer.Screen name="AnexoList" component={AnexoListScreen}/>
            <RightDrawer.Screen name="AnexoForm" component={AnexoFormScreen}/>
            <RightDrawer.Screen name="Home" component={MapaScreen} initialParams={{tela: TELAS.MAPA, firstRun: true}}/>
        </RightDrawer.Navigator>
    );
}
