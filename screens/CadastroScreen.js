import React, {useReducer, useRef} from 'react';
import {Image, StyleSheet, View, Alert, ScrollView} from "react-native";
import {ActivityIndicator, IconButton, Button, Caption, Dialog, TextInput, Title, Text} from "react-native-paper";
import {Inputs, Layout} from "../styles";
import * as DocumentPicker from 'expo-document-picker';
import AppbarTonomapa from "../partials/AppbarTonomapa";
import {SafeAreaView} from "react-native-safe-area-context";
import MunicipioSelect from "../components/MunicipioSelect";
import EstadoSelect from "../components/EstadoSelect";
import {TextInputMask} from 'react-native-masked-text';
import {cpf} from 'cpf-cnpj-validator';
import {pickerStyles} from "../styles/pickers";
import colors from "../assets/colors";
import {AuthContext, carregaCurrentUser, carregaSettings, persisteCurrentUser, persisteSettings} from "../db/api";
import {geraTmpId} from '../utils/utils';
import STATUS from "../bases/status";
import CURRENT_USER from "../bases/current_user";

const pickerSelectStyles = StyleSheet.create(pickerStyles);

export default function CadastroScreen({route, navigation}) {
    const initialState = {
        currentUser: null,
        settings: null,
        municipioReferenciaId: null,
        estadoId: null,
        telefone: null,
        titulo: '',
        labelComunidade: ''
    };

    const reducer = (state, newState) => {
        return {...state, ...newState};
    };
    const [state, setState] = useReducer(reducer, initialState);
    const loading = useRef(true);
    const { signUp } = React.useContext(AuthContext);

    if (!route.params?.loaded) {
        loading.current = true;
        route.params = {
            ...route.params,
            loaded: true
        };
        // navigation.setParams({loaded: true});
    }

    // Load data from local storage
    if (loading.current) {
        loading.current = false;
        const titulosCadastro = {
            titulo: 'Cadastro',
            labelComunidade: 'Comunidade que será mapeada',
            labelSalvar: 'Enviar'
        };
        if (route.name == 'Cadastro') {
            let newCurrentUser = CURRENT_USER;
            let newState = {...titulosCadastro};
            if (route.params?.username) {
                newCurrentUser.username = route.params.username;
                newState.telefone = route.params.username;
                delete route.params.username;
            }
            newState.currentUser = newCurrentUser;
            setState(newState);
        } else {
            carregaCurrentUser().then((currentUser) => {
                if (currentUser) {
                    let newState = {};
                    if (route.name == 'Cadastro') {
                        let estadoId = null;
                        let municipioReferenciaId = null;
                        if (!!currentUser.currentTerritorio.municipioReferenciaId) {
                            estadoId = parseInt(currentUser.currentTerritorio.municipioReferenciaId.toString().substring(0, 2));
                            municipioReferenciaId = parseInt(currentUser.currentTerritorio.municipioReferenciaId);
                        }
                        setState({
                            ...titulosCadastro,
                            currentUser: currentUser,
                            municipioReferenciaId: municipioReferenciaId,
                            estadoId: estadoId
                        });
                    } else {
                        carregaSettings().then(settings => {
                            currentUser.currentTerritorio = CURRENT_USER.currentTerritorio;
                            setState({
                                currentUser: currentUser,
                                settings: settings,
                                titulo: 'Nova comunidade',
                                labelComunidade: 'Nome da comunidade',
                                labelSalvar: 'Criar'
                            });
                        });
                    }
                }
            }).catch((error) => {
                setState({
                    ...titulosCadastro,
                    currentUser: CURRENT_USER
                });
                console.log("Não foi criado este usuário localmente ainda.", error);
            });
        }
    }

    const onChangeEstado = (estadoId) => {
        setState({
            municipioReferenciaId: null,
            estadoId: estadoId
        });
    };

    function validarCadastroBasico() {
        const errors = [];

        if (route.name == 'Cadastro') {
            if (!state.currentUser.username || state.currentUser.username.length < 10) {
                errors.push(['telefone', '· Seu telefone']);
            }

            if (!state.currentUser.fullName || 0 === state.currentUser.fullName.trim().length) {
                errors.push(['nome', '· Seu nome']);
            }

            if (!state.currentUser.cpf || 0 === state.currentUser.cpf.trim().length) {
                errors.push(['cpf', '· CPF obrigatório']);
            }

            if (!cpf.isValid(state.currentUser.cpf)) {
                errors.push(['cpf', '· CPF inválido']);
            }
        }


        if (!state.municipioReferenciaId) {
            errors.push(['municipioReferenciaId', '· Município']);
        }

        if (!state.currentUser.currentTerritorio.nome || 0 === state.currentUser.currentTerritorio.nome.trim().length) {
            errors.push(['nomeComunidade', '· Nome da comunidade que vai ser mapeada']);
        } else if (route.name == 'CadastroTerritorio') {
            let comunidadeJaExiste = false;
            for (let territorio of state.settings.territorios) {
                if (territorio.id != null && territorio.nome === state.currentUser.currentTerritorio.nome) {
                    comunidadeJaExiste = true;
                    break;
                }
            }
            if (comunidadeJaExiste) {
                errors.push(['nomeComunidadeJaExiste', '· Você já está mapeando uma comunidade com este nome. Por favor, escolha outro nome']);
            }
        }

        if (errors.length > 0) {
            let message = "Os seguintes dados precisam ser fornecidos ou alterados:\n\n";
            for (let erro of errors) {
                message += erro[1] + "\n"
            }

            Alert.alert(
                'Dados incompletos ou repetidos',
                message,
                [
                    {
                        text: 'OK', onPress: async () => {
                        }
                    },
                ],
                {cancelable: true},
            );
            return false;
        }

        return true;
    }

    const salvarFn = () => {
        if (validarCadastroBasico()) {
            const currentUser = {
                ...state.currentUser,
                cpf: state.currentUser.cpf.replace(/[\W_]/g, ''),
                currentTerritorio: {
                    ...state.currentUser.currentTerritorio,
                    municipioReferenciaId: state.municipioReferenciaId.toString(),
                    statusId: STATUS.TELEFONE_NAO_REGISTRADO,
                }
            };

            persisteCurrentUser({currentUser: currentUser}).then(() => {
                if (route.name == 'Cadastro') {
                    persisteSettings().then(() => {
                        signUp('new_user');
                    });
                } else {
                    state.settings.territorios.push(currentUser.currentTerritorio);
                    persisteSettings({territorios: state.settings.territorios}).then((res) => {
                        navigation.navigate('Home', {
                            atualizarDados: true,
                            snackBarMessage: 'Você agora está mapeando a comunidade ' + currentUser.currentTerritorio.nome
                        });
                    });
                }
            });
        }
    };

    return (
        <>
        {!state.currentUser &&
            <View style={Layout.appOverlay}>
                <View style={{backgroundColor: colors.primary, padding: 40, alignItems: 'center', borderRadius: 8}}>
                    <ActivityIndicator size="large" animating={true} color={colors.white} />
                    <Text style={{color: colors.white, marginTop: 30}}>Carregando...</Text>
                </View>
            </View>
        }
        {state.currentUser &&
        <View style={Layout.containerStretched}>
            <AppbarTonomapa navigation={navigation} title={state.titulo} goBack={true} />
            <View style={Layout.body}>
                <ScrollView>
                    <SafeAreaView style={Layout.innerBody}>
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu telefone"
                                value={state.telefone}
                                labelStyle={Inputs.textInputLabel}
                                style={Inputs.textInput}
                                onChangeText={text => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        username: text.replace(/[^0-9.]/g, '')
                                    };
                                    setState({
                                        telefone: text,
                                        currentUser: newCurrentUser
                                    });
                                }}
                                render={props => (
                                    <TextInputMask
                                        {...props}
                                        type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }}
                                    />
                                )}
                            />
                        }
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu nome"
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.currentUser.fullName}
                                onChangeText={(value) => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        fullName: value
                                    };
                                    setState({currentUser: newCurrentUser});
                                }}
                            />
                        }
                        {route.name == 'Cadastro' &&
                            <TextInput
                                label="Seu CPF"
                                style={Inputs.textInput}
                                labelStyle={Inputs.textInputLabel}
                                value={state.currentUser.cpf}
                                keyboardType = 'numeric'
                                onChangeText={(value) => {
                                    const newCurrentUser = {
                                        ...state.currentUser,
                                        cpf: cpf.format(value)
                                    };
                                    setState({currentUser: newCurrentUser});
                                }}
                            />
                        }
                        <TextInput label={state.labelComunidade} style={Inputs.textInput}
                                   value={state.currentUser.currentTerritorio.nome} onChangeText={(value) => {
                            const newCurrentUser = {
                                ...state.currentUser,
                                currentTerritorio: {
                                    ...state.currentUser.currentTerritorio,
                                    nome: value
                                }
                            };
                            setState({currentUser: newCurrentUser});
                        }}/>

                        <Caption style={Inputs.caption}>Município de Referência</Caption>
                        <EstadoSelect selectedValue={state.estadoId} onValueChange={onChangeEstado}/>
                        <MunicipioSelect
                            estado={state.estadoId}
                            selectedValue={state.municipioReferenciaId}
                            onValueChange={(value) => { setState({municipioReferenciaId: value}) }}
                        />

                        <View style={Layout.buttonsBottomWrapper}>
                            <View style={(route.name == 'Cadastro') ? Layout.buttonRight : {...Layout.row}}>
                                {(route.name == 'CadastroTerritorio') &&
                                    <Button mode="contained" style={Layout.buttonLeft} color={colors.danger} onPress={() => navigation.goBack()}>Cancelar</Button>
                                }
                                <Button mode="contained" dark={true} style={Layout.buttonRight} color={colors.primary} onPress={salvarFn}>{state.labelSalvar}</Button>
                            </View>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        </View>
        }
        </>
    );
}
